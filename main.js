import http from "k6/http";
import XMLRPC from "./lib/xmlrpc.js"

export default function() {
    let response = http.get("https://test.loadimpact.com");

    let method = "L'Arc-en-Ciel";
    // let params = ['hyde', 'ken', 'tetsuya', 'yukihiro'];
    // let params = [true, false];
    let params = [{'vocal': 'hyde', 'guitar': 'ken', 'bass': 'tetsuya', 'drums': 'yukihiro'}, ['hyde', 'ken', 'tetsuya', 'yukihiro']];

    let xmlrpc = new XMLRPC;
    console.log(xmlrpc.build_call(method, params));
};