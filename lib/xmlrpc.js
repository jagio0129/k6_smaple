import encoding from "k6/encoding";

module.exports = class XMLRPC {
  hoge(){
    console.log('Hello')
  }

  build_call(method, params){
    var xmltext = "";
    function add (s) {
      xmltext += s;
    }

    add ("<?xml version=\"1.0\"?>");
    add ("<methodCall>");
    add ("<methodName>" + method + "</methodName>");
    add ("<params>");

    for (var x in params) {
      add ("<param>");
      add ("<value>" + this.get_xml_value (params [x]) + "</value>")
      add ("</param>");
    }

    add ("</params>");
    add ("</methodCall>");

    return xmltext;
  }

  get_xml_value(value) {
    switch (typeof (value)) {
      case "string":
        return("<string>" + value + "</string>");
      case "boolean":
        return("<boolean>" + this.get_boolean(value) + "</boolean>");
      case "number":
        if (Number.isInteger (value)) {
          return ("<i4>" + value + "</i4>");
        }
        else {
          return ("<double>" + value + "</double>");
        }
      case "object":
        return this.build_type_object(value);
      default:
        return ("<base64>" + encoding.b64encode(value) + "</base64>");
    }
  }

  build_type_object(value){
    var xmltext = "";
    function add (s) {
      xmltext += s;
    }

    if (Array.isArray(value)) {
      add ("<array>");
      add ("<data>");
      for (var i = 0; i < value.length; i++) {
        add ("<value>" + this.get_xml_value(value[i]) + "</value>");
      }
      add ("</data>");
      add ("</array>");
      return (xmltext);
    }else if(value instanceof ArrayBuffer) {
      return ("<base64>" + encoding.b64encode(value) + "</base64>");
    }else if(value instanceof Date) { 
      return ("<dateTime.iso8601>" + value.toISOString () + "</dateTime.iso8601>");
    }else{
      add ("<struct>");
      for (var x in value) {
        add ("<member>");
        add ("<name>" + x + "</name>");
        add ("<value>" + this.get_xml_value(value[x]) + "</value>");
        add ("</member>");
      }
      add ("</struct>");
      return (xmltext);
    }
  }

  get_boolean(value){
    if(value === true) {
      return 1;
    }
    return 0;
  }
}
