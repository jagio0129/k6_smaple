k6 Sample
===

## Setup
```
docker pull loadimpact/k6
```

## Run
```
cd path/to/k6_sample

# for fish
docker run -v (pwd):/src -i loadimpact/k6 run /src/main.js
```

## Ref
- [k6 Docs](https://docs.k6.io/docs)
- [loadimpac/k6 - Github](https://github.com/loadimpact/k6)
- [scripting/xml-rpc](https://github.com/scripting/xml-rpc)
- [XML-RPC Specification 日本語訳](https://www.futomi.com/lecture/japanese/xml_rpc.html)
- [ruby/xmlrpc - Github](https://github.com/ruby/xmlrpc/blob/master/lib/xmlrpc/create.rb)
